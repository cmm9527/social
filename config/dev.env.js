// 'use strict'
// const merge = require('webpack-merge')
// const prodEnv = require('./prod.env')

// module.exports = merge(prodEnv, {
//   NODE_ENV: '"development"'
// })
'use strict'
//新版本用解构出来merge模块，已经不能直接merge，因为依赖的源文件暴露方式也变了
'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"'
  , BASE_URI: '"http://localhost:8080"'
})

