# 校园社交网站(PC端)

## 项目介绍
这是一个前后端分离的项目，项目采用Vue，Node，Mysql，elementUI等组件库开发。

### 项目技术栈

#### 前端技术栈

1. vue
2. elementUI
3. axios
4. vue-router
5. WebSocket
6. vue-cli
7. Echarts
8. ...

#### 后端技术栈

1. node
2. MySQL
3. WebSocket
4. async
5. axios(爬虫)
6. ...


- 项目地址：https://gitee.com/cmm9527/social (前端) 
           </br>&nbsp;&nbsp;        https://gitee.com/cmm9527/social-manage (后端)
           

## 前端项目运行
``` bash
# 克隆到本地
git clone https://gitee.com/cmm9527/social.git

# 进入文件夹
cd campus-social

# 安装依赖
npm install

# 开启本地服务器localhost:8080
npm run dev
```

## 后端项目运行
``` bash
# 克隆到本地
git clone https://gitee.com/cmm9527/social-manage.git

# 进入文件夹
cd campus-social

# 安装依赖
npm install

# 启动express服务器
node app.js
```

## 功能展示

### 登录注册

<img src="./static/exhibition/登录.png" alt="image-20210912160847530" style="zoom:50%;" />

### 个人信息

<img src="./static/exhibition/个人信息.png" alt="image-20210912160657675" style="zoom:50%;" />

### 管理员权限

<img src="./static/exhibition/管理员.png" alt="image-20210912160632094" style="zoom:50%;" />

### 通知

<img src="./static/exhibition/聊天.png" alt="image-20210912160724531" style="zoom:50%;" />

### 动态

<img src="./static/exhibition/动态.png" alt="image-20210912160746001" style="zoom:50%;" />

### 聊天

<img src="./static/exhibition/聊天.png" alt="image-20210912160824880" style="zoom:50%;" />
